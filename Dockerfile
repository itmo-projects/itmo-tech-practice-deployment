FROM alpine:latest

RUN apk update && \
    apk add --no-cache curl && \
    apk add --no-cache bash && \
    curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash && \
    rm -rf /var/cache/apk/*

CMD ["/bin/sh"]