#!/bin/bash
export FOLDER_ID=$(yc config get folder-id)

export SERVICE_ACCOUNT_ID=$(yc iam service-account create --name practice-resource-account \
    --description "Сервисный аккаунт для ресурсов развертывания" \
    --format json \
    | jq -r '.id')

yc resource-manager folder add-access-binding $FOLDER_ID \
  --role admin \
  --subject serviceAccount:$SERVICE_ACCOUNT_ID

### Создать токен для сервисного аккаунта
json_output=$(yc iam access-key create --service-account-id $SERVICE_ACCOUNT_ID --format json)
SERVICE_ACCOUNT_SECRET_KEY=$(echo $json_output | jq -r '.secret')
SERVICE_ACCOUNT_ACCESS_KEY=$(echo $json_output | jq -r '.access_key.key_id')

### Создать функции
export CF1_ID=$(yc serverless function create --name=cf1 --format=json | jq -r '.id')
export CF2_ID=$(yc serverless function create --name=cf2 --format=json | jq -r '.id')
export CF3_ID=$(yc serverless function create --name=cf3 --format=json | jq -r '.id')

cd ./code/http/
cd ./cf1/
yc serverless function version create \
  --function-name cf1 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --environment FUNCTION_ID=$CF2_ID \
  --source-path index.py \
  --async
cd ..

cd ./cf2/
yc serverless function version create \
  --function-name cf2 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --environment FUNCTION_ID=$CF3_ID \
  --source-path index.py \
  --async

cd ..

cd ./cf3/
  yc serverless function version create \
  --function-name cf3 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --source-path index.py \
  --async
cd ../../..

### Создание gateway
sed -e "s/\$CF1_ID/$CF1_ID/g" -e "s/\$SERVICE_ACCOUNT_ID/$SERVICE_ACCOUNT_ID/g" ./deployment/gateway/gateway.template.yml > gateway.yml

export GW1_ID=$(yc serverless api-gateway create \
  --name gw1 \
  --format yaml \
  --spec gateway.yml \
  --format json | jq -r '.id')

rm -f gateway.yml
GW1_DOMAIN=$(yc serverless api-gateway get --id $GW1_ID --format json | jq -r '.domain')
echo "Infractructure is ready on domain $GW1_DOMAIN"