#!/bin/sh

SERVICE_ACCOUNT_ID_ADMIN=ajempmnhfuhlqahfdvev
AWS_DEFAULT_REGION=ru-central1

json_output=$(yc iam access-key create --service-account-id $SERVICE_ACCOUNT_ID_ADMIN --format json)
AWS_SECRET_KEY=$(echo $json_output | jq -r '.secret')
AWS_ACCESS_KEY_ID=$(echo $json_output | jq -r '.access_key.key_id')

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_KEY
aws configure set default.region $AWS_DEFAULT_REGION