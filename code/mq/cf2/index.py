import json
import requests
import boto3
import os
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

def handler(event, context):
    try:
        access_token = context.token['access_token']
        messages = event["messages"]

        queue_url = os.environ['QUEUE_URL']

        queue_client = boto3.client(
            service_name='sqs',
            endpoint_url='https://message-queue.api.cloud.yandex.net',
            region_name='ru-central1'
        )
        
        for message in messages:
            body = message["details"]["message"]["body"]

            request_body = json.loads(body)
            connection_id = request_body["connection_id"]
            ws_message = request_body["ws_message"]

            data = {
                "connection_id": connection_id,
                "ws_message" : ws_message,
                "status" : MessageStatus.PROCESSING.name
            }

            url = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'
            json_ws_data = json.dumps(data)

            http_send_ws_data = {
                "data": base64.b64encode(json_ws_data.encode('utf-8')).decode('utf-8'),
                "type": "TEXT"
            }

            headers = {
                "Authorization": f"Bearer {access_token}"
            }

            response = requests.post(url, json=http_send_ws_data, headers=headers)

            response.raise_for_status()

            result = queue_client.send_message(
                QueueUrl=queue_url,
                MessageBody=json.dumps(data))

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)

        return {
            'statusCode': 500,
            'body': error_message,
            'requestId': 'someRequestId',
        }