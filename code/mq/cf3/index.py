import json
import requests
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

def handler(event, context):
    try:
        messages = event["messages"]
        access_token = context.token['access_token']

        for message in messages:
            body = message["details"]["message"]["body"]

            json_body = json.loads(body)
            
            connection_id = json_body["connection_id"]
            json_body["status"] = MessageStatus.PROCESSED.name

            json_json = json.dumps(json_body)

            url = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'

            ws_data = {
                "data": base64.b64encode(json_json.encode('utf-8')).decode('utf-8'),
                "type": "TEXT"
            }

            headers = {
                "Authorization": f"Bearer {access_token}"
            }

            response = requests.post(url, json=ws_data, headers=headers)

            response.raise_for_status();

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)

        return {
            'statusCode': 500,
            'body': error_message,
            'requestId': 'requestId',
        }