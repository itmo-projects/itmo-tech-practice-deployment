import requests
import os
import boto3
import json
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

## Эту функцию будем вызывать при появлении сообщения в соединении
## Нужна будет переменная среды: QUEUE_URL (url очереди сообщений), AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY (ключи от серисного аккаунта с `ymq.writer`)
## Отправка сообщения в mq1
def handler(event, context):
    requestId = event['requestContext']['requestId']
    try:
        x_serverless_gateway_id = event['headers']['X-Serverless-Gateway-Id']
        x_yc_apigateway_websocket_connection_id = event['headers']['X-Yc-Apigateway-Websocket-Connection-Id']
        source_ip = event['requestContext']['identity']['sourceIp']
        user_agent = event['requestContext']['identity']['userAgent']
        ws_message = event['body']
        connection_id = x_yc_apigateway_websocket_connection_id
        access_token = context.token['access_token']

        data = {
            "connection_id" : connection_id,
            "ws_message" : ws_message,
            "status" : MessageStatus.REGISTERED.name
        }

        queue_url = os.environ['QUEUE_URL']

        url = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'

        json_ws_data = json.dumps(data)

        http_send_ws_data = {
            "data": base64.b64encode(json_ws_data.encode('utf-8')).decode('utf-8'),
            "type": "TEXT"
        }

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response = requests.post(url, json=http_send_ws_data, headers=headers)

        response.raise_for_status()

        queue_client = boto3.client(
            service_name='sqs',
            endpoint_url='https://message-queue.api.cloud.yandex.net',
            region_name='ru-central1'
        )

        result = queue_client.send_message(
            QueueUrl=queue_url,
            MessageBody=json.dumps(data)
        )

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)

        return {
            'statusCode': 500,
            'body': error_message,
            'requestId': requestId,
        }