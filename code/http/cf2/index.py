import requests
import os
import json
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

## Эту функцию будует вызывать cf1 по HTTP
## Нужна будет переменная среды FUNCTION_ID с id фунции в которую мы будем дальше отправлять запрос
## Нужен сервисный аккаунт sa-cf-invoker с правами на вызов функции
def handler(event, context):
    try:
        event_body = event['body']

        data = json.loads(event_body)

        connection_id = data["connection_id"]
        data["status"] = MessageStatus.PROCESSING.name

        access_token = context.token['access_token']

        url_ws = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'

        json_json = json.dumps(data)

        http_send_ws_data = {
            "data": base64.b64encode(json_json.encode('utf-8')).decode('utf-8'),
            "type": "TEXT"
        }

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response2 = requests.post(url_ws, json=http_send_ws_data, headers=headers)

        response2.raise_for_status()


        # Making POST request with Authorization header
        function_id = os.environ.get('FUNCTION_ID')
        url = f'https://functions.yandexcloud.net/{function_id}'

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response = requests.post(url,json=data, headers=headers)

        response.raise_for_status()

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)
     
        return {
            'statusCode': 500,
            'body': error_message
        }