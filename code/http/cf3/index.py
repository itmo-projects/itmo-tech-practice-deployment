import requests
import base64
import json
import os
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

## Эту функцию будует вызывать cf2 по HTTP
## Эта функция будет отправлять сообщения в ws
## Нужен сервисный аккаунт sa-cf-ws-writer с правами на запись в ws
def handler(event, context):
    requestId = event['requestContext']['requestId']
    try:
        event_body = event['body']

        data = json.loads(event_body)

        connection_id = data["connection_id"]
        data["status"] = MessageStatus.PROCESSED.name

        access_token = context.token['access_token']
        
        url = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'

        json_json = json.dumps(data)

        ws_data = {
            "data": base64.b64encode(json_json.encode('utf-8')).decode('utf-8'),
            "type": "TEXT"
        }

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response = requests.post(url, json=ws_data, headers=headers)

        response.raise_for_status()

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)
     
        return {
            'statusCode': 500,
            'body': error_message,
             'requestId': requestId
        }