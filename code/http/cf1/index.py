import requests
import os
import json
import base64
from enum import Enum, auto

class MessageStatus(Enum):
    REGISTERED = auto()
    PROCESSING = auto()
    PROCESSED = auto()

## Эту функцию будем вызывать при появлении сообщения в соединении
## Нужна будет переменная среды FUNCTION_ID с id фунции в которую мы будем отправлять запрос
## Отправка POST -запроса в функцию через HTTP
## Нужен сервисный аккаунт sa-cf-invoker с правами на вызов функции и отправку (websocketWriter)
def handler(event, context):
    requestId = event['requestContext']['requestId']
    try:
        x_serverless_gateway_id = event['headers']['X-Serverless-Gateway-Id']
        x_yc_apigateway_websocket_connection_id = event['headers']['X-Yc-Apigateway-Websocket-Connection-Id']
        source_ip = event['requestContext']['identity']['sourceIp']
        user_agent = event['requestContext']['identity']['userAgent']
        ws_message = event['body']
        connection_id = x_yc_apigateway_websocket_connection_id

        access_token = context.token['access_token']

        # Making POST request with Authorization header
        function_id = os.environ.get('FUNCTION_ID')
        url = f'https://functions.yandexcloud.net/{function_id}'
        
        data = {
            "connection_id" : x_yc_apigateway_websocket_connection_id,
            "ws_message" : ws_message,
            "status" : MessageStatus.REGISTERED.name
        }

        url_ws = f'https://apigateway-connections.api.cloud.yandex.net/apigateways/websocket/v1/connections/{connection_id}:send'

        json_data = json.dumps(data);

        http_send_ws_data = {
            "data": base64.b64encode(json_data.encode('utf-8')).decode('utf-8'),
            "type": "TEXT"
        }

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response2 = requests.post(url_ws, json=http_send_ws_data, headers=headers)

        print(response2)

        response2.raise_for_status()

        headers = {
            "Authorization": f"Bearer {access_token}"
        }

        response1 = requests.post(url,json=data, headers=headers)

        print(response1)

        response1.raise_for_status()

        return {
            'statusCode': 200
        }
    except Exception as e:
        error_message = str(e)

        print(error_message)

        return {
            'statusCode': 500,
            'body': error_message,
            'requestId' : requestId
        }