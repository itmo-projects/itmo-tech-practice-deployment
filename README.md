# ITMO Tech Practice 2 | Вторая технологическая практика ИТМO | Репозиторий автоматизации проведения эксперимента
## Автор : Василий Назаренко


### Предварительная настройка yc
```bash
yc config profile create gitlab-ci
yc config set service-account-key $SA_KEY
yc config set cloud-id $CLOUD_ID
yc config set folder-id $FOLDER_ID
```

### Как запускать
```bash
source ./http-deploy.sh
```
### Как удалять инфраструктуру
```bash
./http-destroy.sh
```


### Prepequisites:
* yc
* jq
* aws