#!/bin/bash

yc serverless api-gateway delete --id $GW1_ID

yc serverless function delete --id $CF1_ID
yc serverless function delete --id $CF2_ID
yc serverless function delete --id $CF3_ID


#### Удаление триггеров
yc serverless trigger delete --id $TR1_ID --async
yc serverless trigger delete --id $TR2_ID --async

### Удаление очередей сообщений
aws sqs delete-queue \
  --endpoint $MQ_ENDPOINT \
  --queue-url $MQ1_URL

aws sqs delete-queue \
  --endpoint $MQ_ENDPOINT \
  --queue-url $MQ2_URL

### Удаляем сервисный аккаунт
yc iam service-account delete --id $SERVICE_ACCOUNT_ID