#!/bin/bash
### Подготовим сервисный аккаунт

export FOLDER_ID=$(yc config get folder-id)

## Создаем сервисный аккаунт для ресурсов которые будем создавать
export SERVICE_ACCOUNT_ID=$(yc iam service-account create --name practice-resource-account-2 \
    --description "Сервисный аккаунт для ресурсов развертывания c очередями" \
    --format json | jq -r '.id')

## Дадим ему админа пока что
yc resource-manager folder add-access-binding $FOLDER_ID \
  --role admin \
  --subject serviceAccount:$SERVICE_ACCOUNT_ID


### Создать токен для сервисного аккаунта

# Инициализируем две переменных среды:
# SERVICE_ACCOUNT_SECRET_KEY - секретный ключ (поле `secret`)
# SERVICE_ACCOUNT_ACCESS_KEY - идентификатор ключа доступа (поле `key_id`)


json_output=$(yc iam access-key create --service-account-id $SERVICE_ACCOUNT_ID --format json)
SERVICE_ACCOUNT_SECRET_KEY=$(echo $json_output | jq -r '.secret')
SERVICE_ACCOUNT_ACCESS_KEY=$(echo $json_output | jq -r '.access_key.key_id')



### Создаем очереди

export MQ_ENDPOINT=https://message-queue.api.cloud.yandex.net/

MQ1_NAME=mq1
export MQ1_URL=$(aws sqs create-queue \
  --queue-name $MQ1_NAME \
  --endpoint $MQ_ENDPOINT | jq -r '.QueueUrl')

MQ2_NAME=mq2
export MQ2_URL=$(aws sqs create-queue \
  --queue-name $MQ2_NAME \
  --endpoint $MQ_ENDPOINT | jq -r '.QueueUrl')


### Создаем функции

export CF1_ID=$(yc serverless function create --name=cf1 --format=json | jq -r '.id')
export CF2_ID=$(yc serverless function create --name=cf2 --format=json | jq -r '.id')
export CF3_ID=$(yc serverless function create --name=cf3 --format=json | jq -r '.id')


### Подготовим архивы для функций

cd ./code/mq/
cd ./cf1/
zip cf1.zip index.py requirements.txt
cd ..
cd ./cf2/
zip cf2.zip index.py requirements.txt
cd ..
cd ./cf3/
zip cf3.zip index.py
cd ../../../


### Загружаем код функций

cd ./code/mq/
cd ./cf1/
yc serverless function version create \
  --function-name cf1 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --environment AWS_ACCESS_KEY_ID=$SERVICE_ACCOUNT_ACCESS_KEY \
  --environment AWS_SECRET_ACCESS_KEY=$SERVICE_ACCOUNT_SECRET_KEY \
  --environment QUEUE_URL=$MQ1_URL \
  --source-path cf1.zip \
  --async
cd ..


cd ./cf2/
yc serverless function version create \
  --function-name cf2 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --environment AWS_ACCESS_KEY_ID=$SERVICE_ACCOUNT_ACCESS_KEY \
  --environment AWS_SECRET_ACCESS_KEY=$SERVICE_ACCOUNT_SECRET_KEY \
  --environment QUEUE_URL=$MQ2_URL \
  --source-path cf2.zip \
  --async
cd ..


cd ./cf3/
yc serverless function version create \
  --function-name cf3 \
  --memory 128m \
  --execution-timeout 5s \
  --runtime python312 \
  --entrypoint index.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --source-path cf3.zip \
  --async
cd ../../

rm ./mq/cf1/cf1.zip
rm ./mq/cf2/cf2.zip
rm ./mq/cf3/cf3.zip
cd ..

### Создадит триггеры для 


# сначала получим ARN очередей
export MQ1_ARN=$(aws sqs get-queue-attributes \
  --endpoint https://message-queue.api.cloud.yandex.net \
  --queue-url $MQ1_URL \
  --attribute-names QueueArn | jq -r '.Attributes.QueueArn' )

  export MQ2_ARN=$(aws sqs get-queue-attributes \
  --endpoint https://message-queue.api.cloud.yandex.net \
  --queue-url $MQ2_URL \
  --attribute-names QueueArn | jq -r '.Attributes.QueueArn' )

export TR1_ID=$(yc serverless trigger create message-queue \
  --name tr1 \
  --queue $MQ1_ARN \
  --queue-service-account-id $SERVICE_ACCOUNT_ID \
  --invoke-function-id $CF2_ID  \
  --invoke-function-service-account-id $SERVICE_ACCOUNT_ID \
  --batch-size 1 \
  --batch-cutoff 1s \
  --format json | jq -r '.id')


export TR2_ID=$(yc serverless trigger create message-queue \
  --name tr2 \
  --queue $MQ2_ARN \
  --queue-service-account-id $SERVICE_ACCOUNT_ID \
  --invoke-function-id $CF3_ID  \
  --invoke-function-service-account-id $SERVICE_ACCOUNT_ID \
  --batch-size 1 \
  --batch-cutoff 1s \
  --format json | jq -r '.id')


### Теперь щлюз

sed -e "s/\$CF1_ID/$CF1_ID/g" -e "s/\$SERVICE_ACCOUNT_ID/$SERVICE_ACCOUNT_ID/g" ./deployment/gateway/gateway.template.yml > gateway.yml

export GW1_ID=$(yc serverless api-gateway create \
  --name gw1 \
  --format yaml \
  --spec gateway.yml \
  --format json | jq -r '.id')

rm -f gateway.yml

GW1_DOMAIN=$(yc serverless api-gateway get --id $GW1_ID --format json | jq -r '.domain')
echo "Infractructure is ready on domain $GW1_DOMAIN"

