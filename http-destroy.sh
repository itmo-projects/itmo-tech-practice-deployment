#!/bin/bash
yc serverless function delete --id $CF1_ID
yc serverless function delete --id $CF2_ID
yc serverless function delete --id $CF3_ID

### Удаляем шлюз
yc serverless api-gateway delete --id $GW1_ID

### Удаляем сервисный аккаунт
yc iam service-account delete --id $SERVICE_ACCOUNT_ID